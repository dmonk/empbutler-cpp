#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "EmpButler.hpp"
#include "tools.hpp"
#include <string>
#include <vector>

TEST_CASE("Test get functions", "[getters]") {
    char *my_argv[] = {
        "./bin/empbutler", // most programs will ignore this
        "-c",
        "~/mp801/serenity_connections.xml",
        "do",
        "x1",
        "info",
        NULL
    };
    EmpButler *e = new EmpButler(6, my_argv);
    REQUIRE(e->getConnectionsFile() == "~/mp801/serenity_connections.xml");
    REQUIRE(e->getCommands()[0] == "do");
    REQUIRE(e->getCommands()[1] == "x1");
    REQUIRE(e->getCommands()[2] == "info");
    // delete e;
}

TEST_CASE("Test getChannelList() functionality") {
    SECTION("Passing 'none' should return an empty vector.") {
        std::string s = "none";
        REQUIRE(getChannelList("none").size() == 0);
    }
    SECTION("Passing a comma separated list should return a vector of those numbers.") {
        std::string s = "1,2,3";
        std::vector<uint32_t> v = getChannelList(s);
        REQUIRE(v.size() == 3);
        REQUIRE(v[1] == 2);
    }
    SECTION("Passing a hypenated list should return a vector of a range.") {
        std::string s = "1-20";
        std::vector<uint32_t> v = getChannelList(s);
        REQUIRE(v.size() == 20);
        REQUIRE(v[19] == 20);
    }
    SECTION("Passing a combination of the above should give a combination.") {
        std::string s = "1,3-20";
        std::vector<uint32_t> v = getChannelList(s);
        REQUIRE(v.size() == 19);
        REQUIRE(v[19] == 20);
        REQUIRE(v[1] == 3);
    }
}
