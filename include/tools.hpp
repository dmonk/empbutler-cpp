#pragma once

#include <stdint.h>
#include <vector>
#include <string>
#include <iostream>
#include <boost/algorithm/string.hpp>

#include "emp/Controller.hpp"


std::vector<uint32_t> getChannelList(std::string channel_string);
std::vector<uint32_t> getChannelList(emp::Controller& controller, std::string channel_string);

