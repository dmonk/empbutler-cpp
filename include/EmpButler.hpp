#pragma once

#include <stdint.h>
#include <iostream>
#include <string>
#include <vector>
#include <boost/program_options.hpp>

#include "emp/ChannelManager.hpp"
#include "emp/Controller.hpp"
#include "emp/CtrlNode.hpp"
#include "emp/utilities/buffers.hpp"
#include "uhal/HwInterface.hpp"
#include "uhal/log/log.hpp"
#include "uhal/ConnectionManager.hpp"
#include "emp/definitions.hpp"
#include "emp/PathConfigurator.hpp"
#include "emp/Orbit.hpp"
#include "emp/logger/logger.hpp"

#include "tools.hpp"

namespace po = boost::program_options;


class EmpButler {
 private:
    std::string connections_file;
    int timeout;
    std::vector<std::string> commands;
    std::string channels;
    std::string inject_file;
    std::string output_path;
    std::string tx_channels;
    std::string rx_channels;
    std::string loopback_mode;

    po::options_description* desc_top;
    po::options_description* desc_do;
    po::options_description* desc_cmd;
    po::options_description* desc_buffers;
    po::options_description* desc_composite;
    po::options_description* desc_capture;
    po::options_description* desc_mgts;
    po::variables_map vm;

    uhal::ConnectionManager* manager;

    void conflicting_options(const char* opt1, const char* opt2);
    void parseInput(int argc, char **argv);
    void runDo(void);
    void runReset(emp::Controller& controller);
    void runBuffers(emp::Controller& controller);
    void runMGTs(emp::Controller& controller);
    void runCapture(emp::Controller& controller);
    void runInfo(emp::Controller& controller);

 public:
    EmpButler(int argc, char **argv);
    ~EmpButler(void);

    void run(void);

    std::string getConnectionsFile(void);
    std::vector<std::string> getCommands(void);
};
