#include "main.hpp"


int main(int argc, char *argv[]) {
    EmpButler *empbutler = new EmpButler(argc, argv);
    empbutler->run();

    delete empbutler;
    return 0;
}
