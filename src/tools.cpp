#include "tools.hpp"


std::vector<uint32_t> getChannelList(std::string channel_string) {
    std::vector<uint32_t> channel_list;
    if (channel_string == "none") {
        return channel_list;
    } else {
        std::vector<std::string> split_vector;
        boost::split(split_vector, channel_string, boost::is_any_of(","));
        for (auto i : split_vector) {
            std::vector<std::string> range_vector;
            boost::split(range_vector, i, boost::is_any_of("-"));
            if (range_vector.size() == 1) {
                channel_list.push_back((uint32_t)std::stoi(i));
            } else if (range_vector.size() == 2) {
                for (uint32_t i = (uint32_t)std::stoi(range_vector[0]); i < (std::stoi(range_vector[1]) + 1); i++) {
                    channel_list.push_back(i);
                }
            }
        }
        return channel_list;
    }
}

std::vector<uint32_t> getChannelList(emp::Controller& controller, std::string channel_string) {
    std::vector<uint32_t> channel_list;
    if (channel_string == "all") {
        for (uint32_t i = 0; i < 4*controller.getGenerics().nRegions; i++) {
            channel_list.push_back(i);
        }
        return channel_list;
    } else {
        return getChannelList(channel_string);
    }
}

