#include "EmpButler.hpp"

EmpButler::EmpButler(int argc, char **argv) {
    desc_top = new po::options_description("General options");
    desc_cmd = new po::options_description("Command options");
    desc_do = new po::options_description("General do options");
    desc_buffers = new po::options_description("Buffers options");
    desc_capture = new po::options_description("Capture options");
    desc_mgts = new po::options_description("MGTs options");
    desc_composite = new po::options_description("All options");
    tx_channels = "none";
    rx_channels = "none";
    channels = "none";
    output_path = "data";
    parseInput(argc, argv);
}

EmpButler::~EmpButler(void) {
    delete desc_top;
    delete desc_do;
    delete desc_cmd;
    delete desc_buffers;
    delete desc_capture;
    delete desc_composite;
    delete manager;
}

/* Function used to check that 'opt1' an 'opt2' are not specified
   at the same time. */
void EmpButler::conflicting_options(const char* opt1, const char* opt2) {
    if (vm.count(opt1) && !vm[opt1].defaulted() && vm.count(opt2) && !vm[opt2].defaulted()) {
        throw std::logic_error(std::string("Conflicting options '")
                               + opt1 + "' and '" + opt2 + "'.");
    }
}

void EmpButler::parseInput(int argc, char **argv) {
    desc_top->add_options()
    ("help,h", "Show this message and exit.")
    ("connections,c", po::value(&connections_file), "Path to uhal connection file(s)")
    ("timeout,t", po::value(&timeout), "uhal timeout (sec)")
    ("verbose,v", "Verbose mode")
    ("quiet,q", "Quiet mode");
    desc_cmd->add_options()
    ("commands",  po::value(&commands), "Commands to be executed.");
    desc_do->add_options()
    ("chans", po::value(&channels), "Channel list");
    desc_buffers->add_options()
    ("inject", po::value(&inject_file), "File URI of data")
    ("bx-range", "Bx range")
    ("frame-range", "Frame range");
    desc_capture->add_options()
    ("out,o", po::value(&output_path), "Output path")
    ("caplen", "Capture length")
    ("tx", po::value(&tx_channels), "Tx channels to capture")
    ("rx", po::value(&rx_channels), "Rx channels to capture");
    desc_mgts->add_options()
    ("loopback", po::value(&loopback_mode), "Loopback mode")
    ("invert", "Invert polarity")
    ("enable-dfe", "Enables DFE");
    desc_composite->add(*desc_top).add(*desc_do).add(*desc_cmd).add(*desc_buffers).add(*desc_capture).add(*desc_mgts);
    po::positional_options_description positionalOptions;
    positionalOptions.add("commands", -1);

    try {
        po::store(po::command_line_parser(argc, argv).options(*desc_composite).positional(positionalOptions).run(), vm);
        po::notify(vm);
    } catch(po::error& e) {
        std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
        std::cerr << *desc_composite << std::endl;
        return;
    }
}

void EmpButler::run(void) {
    try {
        if (vm.count("help") || vm.size() == 0) {
            std::cout << "Usage: empbutler [OPTIONS] COMMAND [ARGS]...\n\n";
            std::cout << *desc_top << "\n";
            return;
        }

        conflicting_options("verbose", "quiet");

        // Set verbosity level.
        if (vm.count("verbose")) {
            emp::logger::setLevel("trace");
        } else {
            uhal::disableLogging();
            if (vm.count("quiet")) {
                emp::logger::setLevel("info");
            } else {
                emp::logger::setLevel("debug");
            }
        }

        // Manage connections file.
        if (vm.count("connections")) {
            // Set connections file
            if (!boost::find_first(connections_file, "://")) {
                connections_file.insert(0, "file://");
            }
            manager = new uhal::ConnectionManager(connections_file);
        }

        if (commands.size() == 0) {
            return;
        } else {
            if (commands[0] == "list") {
                // TODO(david): List known uhal devices.
                std::cout << "List known uhal devices.\nempbutler commands:" << '\n';
                std::cout << "-----------------------------------------------------" << '\n';
                std::cout << "for lId in obj.mConnectionManager.getDevices():\n\techo(`` - '{}'``.format(lId))" << '\n';
                std::cout << "-----------------------------------------------------" << '\n';
            } else if (commands[0] == "do") {
                runDo();
            } else {
                std::cout << "Usage: empbutler [OPTIONS] COMMAND [ARGS]...\n\n";
                std::cout << *desc_top << "\n";
                return;
            }
        }
    } catch(po::error& e) {
        std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
        std::cerr << *desc_composite << std::endl;
        return;
    }
}


void EmpButler::runDo(void) {
    enum string_code {
        ereset,
        ebuffers,
        emgts,
        ecapture,
        einfo,
        enull
    };
    auto hashit = [](std::string inString) -> string_code {
        if (inString == "reset") return ereset;
        if (inString == "buffers") return ebuffers;
        if (inString == "mgts") return emgts;
        if (inString == "capture") return ecapture;
        if (inString == "info") return einfo;
        return enull;
    };

    std::string description(
        "Usage: empbutler do DEVICE COMMAND [ARGS]...\n\n"
        "  Execute commands on EMP devices\n\n"
        "Commands:\n"
        "  buffers        Configure datapath buffers\n"
        "  capture        Capture I/O data using datapath buffers\n"
        "  info\n"
        "  inspect        Print snapshot of board registers\n"
        "  mgts\n"
        "  reset          Configure clock and TTC sources, and issue reset\n"
        "  ttc-capture    Capture TTC commands\n"
        "  ttc-check      Check status of TTC block\n"
        "  ttc-scanphase  Scan phase used for external TTC stream\n");

    if (commands.size() == 1) {
        std::cout << description << '\n';
        std::cout << *desc_do << '\n';
        return;
    }

    if (commands.size() > 2) {
        if (connections_file.empty()) {
            std::cout << "Please specifiy a connections file using the -c flag to execute these commands." << '\n';
            return;
        }

        uhal::HwInterface device = manager->getDevice(commands[1]);
        emp::Controller controller(device);
        std::cout << "Created device " << commands[1] << '\n';

        switch (hashit(commands[2])) {
            case ereset:
                // Reset TTC.
                if (commands.size() <= 3) {
                    std::cout << "Please specify whether the reset should be internal or external" << '\n';
                } else {
                    runReset(controller);
                }
                break;
            case ebuffers:
                // Configure buffers.
                if (commands.size() <= 3) {
                    std::cout << "Incomplete command" << '\n';
                    std::cout << "Usage: empbutler do DEVICE buffers [OPTIONS] [rx|tx] [Pattern3G|Pattern|Zeroes|Latency|PlayOnce|PlayOnceStrobe|PlayLoop|Capture|PlayOnce3G|CaptureStrobe]\n\n";
                    std::cout << "  Configure datapath buffers" << "\n\n";
                    std::cout << *desc_do << '\n';
                    std::cout << *desc_buffers << '\n';
                } else {
                    runBuffers(controller);
                }
                break;
            case emgts:
                // Comfigure the MGTs.
                if (commands.size() <= 3) {
                    std::cout << "Configure MGTs" << '\n';
                } else {
                    runMGTs(controller);
                }
                break;
            case ecapture:
                // Capture data.
                runCapture(controller);
                break;
            case einfo:
                // Get information on the firmware.
                runInfo(controller);
                break;
            case enull:
            default:
                return;
        }
    } else {
        std::cout << "Either device or command not specified. \n" << '\n';
        std::cout << description << '\n';
        std::cout << *desc_do << '\n';
    }
}

void EmpButler::runReset(emp::Controller& controller) {
    std::cout << "Resetting devivce '" << commands[1] << "'\n";
    std::cout << "Changing clock and TTC source to '" << commands[3] << "'\n";
    controller.getCtrl().softReset();
    bool external = commands[3] == "external";
    controller.selectClockAndTtcSource(external);
    controller.checkTTC();
}

void EmpButler::runBuffers(emp::Controller& controller) {
    auto hashit = [](std::string inString) -> emp::PathConfigurator::Mode {
        if (inString == "Latency") return emp::PathConfigurator::kLatency;
        if (inString == "Capture") return emp::PathConfigurator::kCapture;
        if (inString == "PlayOnce") return emp::PathConfigurator::kPlayOnce;
        if (inString == "PlayLoop") return emp::PathConfigurator::kPlayLoop;
        if (inString == "Pattern") return emp::PathConfigurator::kPattern;
        if (inString == "Zeroes") return emp::PathConfigurator::kZeroes;
        // if (inString == "CaptureStrobe") return emp::PathConfigurator::kCaptureStrobe;
        // if (inString == "Pattern3G") return emp::PathConfigurator::kPattern3G;
        // if (inString == "PlayOnceStrobe") return emp::PathConfigurator::kPlayOnceStrobe;
        // if (inString == "PlayOnce3G") return emp::PathConfigurator::kPlayOnce3G;
        return emp::PathConfigurator::kPlayOnce;
    };
    emp::RxTxSelector kind = (commands[3] == "tx") ? emp::kTx : emp::kRx;
    if (!boost::find_first(inject_file, "://") && !inject_file.empty()) {
        inject_file.insert(0, "file://");
    }
    std::cout << "Configuring " << commands[3] << " buffers [" << channels << "] in " << commands[4] << " mode." << '\n';
    emp::configureBuffers(
        controller, kind, getChannelList(controller, channels), hashit(commands[4]),
        inject_file, emp::orbit::Point(0, 0));
}

void EmpButler::runMGTs(emp::Controller& controller) {
    emp::ChannelManager channelMgr(controller, getChannelList(controller, channels));
    if (commands[3] == "configure") {
        if (commands[4] == "tx") {
            auto hashit = [](std::string inString) -> emp::MGTchannel::LoopbackMode {
                if (inString == "off") return emp::MGTchannel::kNoLoopback;
                if (inString == "nearPCS") return emp::MGTchannel::kNearPCS;
                if (inString == "nearPMA") return emp::MGTchannel::kNearPMA;
                if (inString == "farPCS") return emp::MGTchannel::kFarPCS;
                if (inString == "FarPMA") return emp::MGTchannel::kFarPMA;
                return emp::MGTchannel::kNoLoopback;
            };
            bool invert = vm.count("invert");
            channelMgr.configureTxMGTs(invert, hashit(loopback_mode));
        } else if (commands[4] == "rx") {
            bool invert = vm.count("invert");
            bool dfe = vm.count("enable-dfe");
            channelMgr.configureRxMGTs(invert, dfe);
        }
    } else if (commands[3] == "status") {
        emp::checkTxMGTs(channelMgr.readTxStatus());
        emp::checkRxMGTs(channelMgr.readRxStatus());
    } else if (commands[3] == "align") {
        channelMgr.minimizeAndAlign(3);
    }
}

void EmpButler::runCapture(emp::Controller& controller) {
    emp::captureAndSaveToFile(
        controller, 0, getChannelList(controller, rx_channels),
        getChannelList(controller, tx_channels), output_path);
}

void EmpButler::runInfo(emp::Controller& controller) {
    emp::CtrlNode ctrl_node = controller.getCtrl();
    std::cout << "Design: 0x" << std::hex << ctrl_node.readDesign() << '\n';
    std::cout << "Firmware revision: " << std::hex << ctrl_node.readFwRevision() << '\n';
    std::cout << "Algorithm version: 0x" << std::hex << ctrl_node.readAlgoRevision() << '\n';
}

std::string EmpButler::getConnectionsFile(void) {
    return connections_file;
}

std::vector<std::string> EmpButler::getCommands(void) {
    return commands;
}
