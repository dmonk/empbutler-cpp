#!/bin/bash

#

COL_RED="\e[31m"
COL_GREEN="\e[32m"
COL_YELLOW="\e[33m"
COL_BLUE="\e[34m"
COL_NULL="\e[0m"

#------------------------------------------------------------------------------
function pathadd() {
  # Assert that we got enough arguments
  if [[ $# -ne 2 ]]; then
    echo "drop_from_path: needs 2 arguments"
    return 1
  fi
  PATH_NAME=$1
  PATH_VAL=${!1}
  PATH_ADD=$2

  # Add the new path only if it is not already there
  if [[ ":$PATH_VAL:" != *":$PATH_ADD:"* ]]; then
    # Note
    # ${PARAMETER:+WORD}
    #   This form expands to nothing if the parameter is unset or empty. If it
    #   is set, it does not expand to the parameter's value, but to some text
    #   you can specify
    PATH_VAL="$PATH_ADD${PATH_VAL:+":$PATH_VAL"}"

    # echo "- $PATH_NAME += $PATH_ADD"
    echo -e "${COL_BLUE}Added ${PATH_ADD} to ${PATH_NAME}${COL_NULL}"

    # use eval to reset the target
    eval "${PATH_NAME}=${PATH_VAL}"
  fi
}
#------------------------------------------------------------------------------


#------------------------------------------------------------------------------
declare -a missing_pypkg
missing_pypkg=()

function chkpypkg() {
  if python -c "import pkgutil; raise SystemExit(1 if pkgutil.find_loader('${1}') is None else 0)" &> /dev/null; then
    echo "${1} is installed"
else
    echo "Error: package '${1}' is not installed"
    missing_pypkg+=(${1})
fi
}
#------------------------------------------------------------------------------

chkpypkg uhal
chkpypkg click
chkpypkg click_didyoumean

(( ${#missing_pypkg[@]} > 0 )) &&  return 1
unset missing_pypkg

SH_SOURCE=${BASH_SOURCE}
EMPTOOLS_ROOT=$(cd $(dirname ${SH_SOURCE}) && pwd)

pathadd LD_LIBRARY_PATH /opt/cactus/lib
pathadd LD_LIBRARY_PATH ${EMPTOOLS_ROOT}/core/lib
pathadd LD_LIBRARY_PATH ${EMPTOOLS_ROOT}/logger/lib
pathadd PYTHONPATH ${EMPTOOLS_ROOT}/python/pkg
pathadd PATH ${EMPTOOLS_ROOT}/python/scripts

export PATH
export LD_LIBRARY_PATH
export PYTHONPATH
export EMPTOOLS_ROOT
export EMP_CONNECTIONS_FILE=${EMPTOOLS_ROOT}/examples/c.xml

# logging
## only log info messages and above (e.g. no debug or trace messages)
#export EMP_CACTUS_LOGLEVEL=INFO
# create a daily log file, e.g. EMP.log
export EMP_CACTUS_DAILY_LOGFILE=ON
# increase the log level to DEBUG or TRACE for the log file if needed
export EMP_CACTUS_DAILY_LOGFILE_LOGLEVEL=INFO


eval "$(_EMPBUTLER_COMPLETE=source empbutler)"
