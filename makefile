CXX := g++
#CXX := arm-linux-gnueabihf-g++
SRCDIR := src
BUILDDIR := build
LIBDIR := lib
TARGET := bin/empbutler

include envserenity

CXX_SRCEXT := cpp
CXX_SOURCES := $(shell find $(SRCDIR) -type f -name *.$(CXX_SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(CXX_SOURCES:.$(CXX_SRCEXT)=.o))
MAIN := $(BUILDDIR)/main.o
CXXFLAGS := -std=c++11 -fPIC
INC := -Iinclude -I$(EMP_ROOT)core/include -I$(CACTUS_ROOT)include -I$(EMP_ROOT)logger/include
LIB := -lboost_program_options -lboost_system
LIB += -L$(EMP_ROOT)logger/lib -lcactus_emp_logger
LIB += -L$(EMP_ROOT)core/lib -lcactus_emp
LIB += -L$(CACTUS_ROOT)lib -lcactus_uhal_uhal -lcactus_uhal_log -lpugixml -lcactus_uhal_grammars
LIB += -L$(shell pwd)/lib


all: lib
all: executable


debug: CXXFLAGS += -DDEBUG -g
debug: lib
debug: executable


executable: $(MAIN)
	@echo "Linking..."
	@echo "$(CXX) $^ -o $(TARGET) $(LIB) -lempbutler"; $(CXX) $^ -o $(TARGET) $(LIB) -lempbutler

lib: $(filter-out $(MAIN), $(OBJECTS))
	@echo "Building library..."
	@mkdir -p $(LIBDIR)
	@echo "$(CXX) -shared -fPIC -o $(LIBDIR)/libempbutler.so $^ $(LIB)"; $(CXX) -shared -fPIC -o $(LIBDIR)/libempbutler.so $^ $(LIB)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(CXX_SRCEXT)
	@mkdir -p $(BUILDDIR)
	@echo "$(CXX) $(CXXFLAGS) $(INC) -c -o $@ $<"; $(CXX) $(CXXFLAGS) $(INC) -c -o $@ $<

install:
	@echo "Copying to /opt/catcus/lib..."
	@echo "cp lib/libempbutler.so /opt/cactus/lib/"; cp lib/libempbutler.so /opt/cactus/lib/	

clean:
	@echo "Cleaning...";
	@echo "$(RM) -r $(BUILDDIR) $(TARGET)"; $(RM) -r $(BUILDDIR) $(TARGET)
	@echo "$(RM) include/*.gch"; $(RM) include/*.gch
	@echo "$(RM) tests/a.out"; $(RM) tests/a.out
	@echo "$(RM) -r $(LIBDIR)"; $(RM) -r $(LIBDIR)

test: lib
test:
	@echo "Builing unittests..."
	@echo "$(CXX) $(CXXFLAGS) -Wno-write-strings -Itests $(INC) tests/test.cpp -o tests/a.out $(LIB) $(LIBDIR)/libempbutler.so"; $(CXX) $(CXXFLAGS) -Wno-write-strings -Itests $(INC) tests/test.cpp -o tests/a.out $(LIB) $(LIBDIR)/libempbutler.so
